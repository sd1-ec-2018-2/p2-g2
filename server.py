from flask import Flask, render_template, session, request, redirect, url_for
from flask_socketio import SocketIO
from flask_socketio import send, emit, join_room
from irc_client import IRCClient
import os
import sys
import json
from time import gmtime, strftime
import time

import string
import random

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)
socketio = SocketIO(app)
IRC_map = {}

@app.route('/', methods = ['GET'])
def index():
    if 'nome' in session:
        return render_template('index.html')
    else:
        return render_template('login.html')
    """session['nome'] = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    session['canal'] = "#ufg"
    session['servidor'] = "localhost"
    try:
        IRC_map[session['nome']] = IRCClient(target=session['canal'], callback=send_message_client, ip=session['servidor'], nickname=session['nome'])
    except Exception:
       print('Falha na conexao com o servidor IRC')
       session.pop('nome', None)

    return render_template('index.html')"""

@app.route('/login', methods = ['POST'])
def login():
    session['nome'] = request.form['nome']
    session['canal'] = request.form['canal']
    session['servidor'] = request.form['servidor']
    try:
        IRC_map[session['nome']] = IRCClient(target=session['canal'], callback=send_message_client, ip=session['servidor'], nickname=session['nome'])
    except Exception:
       print('Falha na conexao com o servidor IRC')
       session.pop('nome', None)

    return redirect('/')

@app.route('/logout')
def logout():
    if 'nome' in session:
        session.pop('nome', None)
    return redirect('/')

def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')

def send_message_client(msg, event_name, room):
    print('enviando para o cliente: ',msg)
    print('room: ',room)
    socketio.emit(event_name, msg, room=str(room))

@socketio.on('connect')
def connect():
    print('sid',request.sid)
    irc_client = IRC_map[session['nome']]
    irc_client.sid = request.sid
    join_room(str(request.sid)) #cria uma room exlusiva para cada usuário

@socketio.on('send_message')
def send_message(data):
    if(data['message'][0] == '/'):
        data = data['message'].split()
        parser(data[0][1:], data, request.sid)
    else:
        irc_client = IRC_map[session['nome']]
        irc_client.connection.privmsg(session['canal'], data['message'])
        data['nick'] = irc_client.nickname
        data['target'] = irc_client.target
        data["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
        send_message_client(data, 'recived_message', request.sid)

@app.route('/info')
def info():
    info = {}
    irc_client = IRC_map[session['nome']]
    info['nick'] = irc_client.nickname
    info['target'] = irc_client.target
    info['server'] = irc_client.ip
    return json.dumps(info)

def parser(command, args, sid):
    command = command.lower()
    irc_client = IRC_map[session['nome']]
    if(command == 'nick'):
        if(len(args)>=2):
            print('nick arg: ', args[1])
            irc_client.connection.nick(args[1])
            irc_client.nickname = args[1]
        else:
            send_message_client('Parametros insuficientes para comando /nick', 'server_message', sid)
    elif(command == 'user'):
        if(len(args)>=3):
            print('user args: ', args)
            irc_client.connection.user(args[1], args[2])
        else:
            send_message_client('Parametros insuficientes para comando /user', 'server_message', sid)
    elif(command == 'quit'):
        irc_client.connection.quit()
        socketio.emit('quit', room=str(sid))
    elif(command == 'mode'):
        if(len(args)>=3):
            print('mode args', args)
            if(len(args)==3):
                irc_client.connection.mode(args[1],args[2])
                send_message_client('MODE '+args[1]+": "+args[2], 'server_message', sid)
            else:
                irc_client.connection.mode(args[1],args[2] +" "+ args[3])
                send_message_client('MODE '+args[1]+": "+args[2] +" "+ args[3], 'server_message', sid)
        else:
            send_message_client('Parametros insuficientes para comando /mode', 'server_message', sid)
    elif(command == "join"):
        if(len(args)>=2):
            if(args[1][0] == '#'):
                print('joins arg: ', args[1])
                irc_client.connection.join(args[1])
                irc_client.target = args[1]
                irc_client.channels.append(args[1])
            else:
                send_message_client('Target inválido', 'server_message', sid)
        else:
            send_message_client('Parametros insuficientes para comando /nick', 'server_message', sid)
    elif(command == "part"):
        if(len(args)>=2):
            message = ""
            if(len(args)>=3):
                message = " ".join(args[2:])
            channels = args[1].split(',')
            print('joins arg: ', args)
            irc_client.connection.part(channels, message=message)
            irc_client.channels = [x for x in irc_client.channels if x not in channels]
            if(irc_client.target in channels):
                if(len(irc_client.channels) == 0):
                    socketio.emit('quit', room=str(sid))
                else:
                    irc_client.target = irc_client.channels[0]
            print('channels on irc:', irc_client.channels)
        else:
            irc_client.connection.part(irc_client.channels)
            socketio.emit('quit', room=str(sid))
    elif(command == "topic"):
        if(len(args)>=2):
            if(args == 2):
                irc_client.connection.topic(args[1])
            else:
                irc_client.connection.topic(args[1], new_topic=" ".join(args[2:]))
        else:
            send_message_client('Parametros insuficientes para comando /topic', 'server_message', sid)
    elif(command == "names"):
        if(len(args)>=2):
            channels_list = args[1].split(',')
        else:
            channels_list = irc_client.channels

        for channel in channels_list:
            send_message_client("Lista de Canais:", 'server_message', sid)
            irc_client.connection.names(channels=channel)
    elif(command == "list"):
        if(len(args)>=2):
            channels_list = args[1].split(',')
        else:
            channels_list = irc_client.channels

        for channel in channels_list:
            irc_client.connection.list(channels=channel)
    elif(command == "invite"):
        if(len(args)>=3):
            irc_client.connection.invite(args[1],args[2])
        else:
            send_message_client('Parametros insuficientes para comando /invite', 'server_message', sid)
    elif(command == "kick"):
        if(len(args)>=3):
            if(len(args)==3):
                irc_client.connection.kick(args[1],args[2])
            else:
                irc_client.connection.kick(args[1],args[2],comment=" ".join(args[3:]))
        else:
            send_message_client('Parametros insuficientes para comando /kick', 'server_message', sid)
    elif(command == "privmsg"):
        if(len(args)>=3):
            irc_client.connection.privmsg(args[1]," ".join(args[2:]))
            msg = {}
            msg["nick"] = irc_client.nickname
            msg["target"] = args[1]
            msg["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
            msg["message"] = " ".join(args[2:])
            send_message_client(json.loads(json.dumps(msg)), 'recived_message', sid)
        else:
            send_message_client('Parametros insuficientes para comando /privmsg', 'server_message', sid)
    elif(command == "notice"):
        if(len(args)>=3):
            irc_client.connection.notice(args[1]," ".join(args[2:]))
            msg = {}
            msg["nick"] = irc_client.nickname
            msg["target"] = args[1]
            msg["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
            msg["message"] = " ".join(args[2:])
            send_message_client(json.loads(json.dumps(msg)), 'recived_message', sid)
        else:
            send_message_client('Parametros insuficientes para comando /notice', 'server_message', sid)
    elif(command == 'motd'):
        IRC_map[session['nome']].connection.motd()
    elif(command == 'lusers'):
        if(len(args) == 1):
            irc_client.connection.lusers()
        else:
            irc_client.connection.lusers(server=args[1])
    elif(command == 'version'):
        if(len(args) == 1):
            irc_client.connection.version()
        else:
            irc_client.connection.version(server=args[1])
    elif(command == 'stats'):
        if(len(args) >= 2):
            if(len(args) == 2):
                irc_client.connection.stats(args[1])
            else:
                irc_client.connection.stats(args[1],server=args[2])
        else:
            send_message_client('Parametros insuficientes para comando /stats', 'server_message', sid)
    elif(command == 'time'):
        if(len(args) == 1):
            irc_client.connection.time()
        else:
            irc_client.connection.time(server=args[1])
    elif(command == 'who'):
        if(len(args)>=2):
            if(len(args)==2):
                irc_client.connection.who(target=args[1])
            else:
                irc_client.connection.who(target=args[1],op=args[2])
        else:
            send_message_client('Parametros insuficientes para comando /who', 'server_message', sid)
    elif(command == 'whois'):
        if(len(args) >= 2):
            irc_client.connection.whois(" ".join(args[1:]))
        else:
            send_message_client('Parametros insuficientes para comando /whois', 'server_message', sid)
    elif(command == 'help'):
        send_message_client(open('help.txt').read(), 'server_message', sid)


if __name__ == '__main__':
    socketio.run(app, debug=True)