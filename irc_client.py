import irc.client
import itertools
import sys
import threading
import time
import json
from time import gmtime, strftime

"""
A lista de envontos possiveis está em https://github.com/jaraco/irc/blob/master/irc/events.py
basta adcionar uma fanção com o prefixo on_ na classe
a função é invocada como se fose um callback
"""


class IRCClient(irc.client.SimpleIRCClient):
    def __init__(self, target, callback, ip='localhost', port=6667, nickname="ircclient"):
        irc.client.SimpleIRCClient.__init__(self)
        self.callback = callback
        self.target = target
        self.ip = ip
        self.nickname = nickname
        print(ip,port,nickname)
        self.connect(ip,port,nickname)
        self.connected = False
        self.sid = None
        self.channels = [target]
        self.run()

    def on_welcome(self, connection, event):
        print('on_welcome', event)
        self.send_server_message_client('Conectado.')
        self.send_server_message_client(event.arguments[0])
        if irc.client.is_channel(self.target):
            connection.join(self.target)
        self.connected = True

    def on_privmsg(self, c, e):
        print('on_privmsg:', e)
        self.send_message_client(e)
        
    def on_pubmsg(self, c, e):
        print('on_pubmsg:', e)
        self.send_message_client(e)

    def on_privnotice(self, c, e):
        print('on_privnotice:', e)
        self.send_message_client(e)
    
    def on_pubnotice(self, c, e):
        print('on_pubnotice:', e)
        self.send_message_client(e)

    def on_mode(self,c,e):
        print('on_mode:', e)
        self.send_server_message_client("MODE "+ str(e.source).split('!')[0]+" - "+str(e.target)+": "+" ".join(e.arguments))
    def on_action(self,c,e):
        print('on_action:', e)

    def on_motd(self,c,e):
        print('on_motd:', e)
        self.send_server_message_client(e.arguments[0])

    def on_nick(self,c,e):
        print('on_nick:', e)
        self.send_server_message_client(str(e.source).split('!')[0]+' agora é conhecido como: '+str(e.target))

    def on_join(self,c,e):
        print('on_join:', e)
        self.send_server_message_client(str(e.source).split('!')[0]+' entrou em: '+str(e.target))

    def on_alreadyregistered(self,c,e):
        e.target = " "
        self.send_server_message_client(e[0])

    def on_quit(self,c,e):
       self.send_message_client(e)

    def on_part(self,c,e):
        print('on_part:', e)
        self.send_server_message_client(str(e.source).split('!')[0]+' saiu de: '+str(e.target))

    def on_namreply(self,c,e):
        print('on_namreply:', e)
        self.send_server_message_client('Users em '+e.arguments[1]+": "+e.arguments[2])

    def on_list(self,c,e):
        print('on_list:', e)
        self.send_server_message_client('Canal: '+e.arguments[0]+" - Usuarios: "+e.arguments[1]+" - Tópico:"+e.arguments[2])

    def on_topic(self,c,e):
        print('on_topic:', e)
        self.send_server_message_client('O usuario: '+str(e.source).split('!')[0]+" mudou o tópico de "+str(e.target)+" para \""+e.arguments[0]+"\"")

    def on_whoreply(self,c,e):
        print('on_who:', e)
        self.send_server_message_client(" - ".join(e.arguments))
    
    def on_umodeis(self,c,e):
        print('on_umodeis', e)

    def on_umodeunknownflag(self,c,e):
        print('on_umodeunknownflag', e)
        self.send_server_message_client("Error: " + e.arguments[0])
    def on_channelmodeis(self,c,e): 
        print('on_channelmodeis', e)
    def on_unknownmode(self,c,e): 
        print('on_unknownmode', e)
    def on_nochanmodes(self,c,e): 
        print('on_nochanmodes', e)

    def on_inviting(self,c,e):
        print('on_inviting', e)
        msg = {}
        msg["nick"] = e.source
        msg["target"] = e.target
        msg["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
        msg["message"] = "{0} invited to {1}".format(e.arguments[0],e.arguments[1])
        self.callback(json.loads(json.dumps(msg)), 'recived_message', self.sid)

    def on_kick(self,c,e):
        print('on_kick', e)
        self.send_server_message_client(str(e.source).split('!')[0] + " expulsou "+e.arguments[0]+" de "+str(e.target))

    def on_luserconns(self,c,e):
        print('on_luserconns', e)
        self.send_message_client(e)
    def on_luserclient(self,c,e):
        print('on_luserclient', e)
        self.send_message_client(e)
    def on_luserchannels(self,c,e):
        print('on_luserchannels', e)
        self.send_message_client(e)
    def on_luserme(self,c,e):
        print('on_luserme', e)
        self.send_message_client(e)

    def on_luserop(self,c,e):
        print('on_luserop', e)
    def on_luserunknown(self,c,e):
        print('on_luserunknown', e)

    def on_version(self,c,e):
        print('on_version', e)
        self.send_message_client(e)

    def on_statslinkinfo(self,c,e):
        print('on_statslinkinfo', e)
        self.send_message_client(e)
    def on_statscommands(self,c,e):
        print('on_statscommands', e)
        self.send_message_client(e)
    def on_statscline(self,c,e):
        print('on_statscline', e)
        self.send_message_client(e)
    def on_statsnline(self,c,e):
        print('on_statsnline', e)
        self.send_message_client(e)
    def on_statsiline(self,c,e):
        print('on_statsiline', e)
        self.send_message_client(e)
    def on_statskline(self,c,e):
        print('on_statskline', e)
        self.send_message_client(e)
    def on_statsqline(self,c,e):
        print('on_statsqline', e)
        self.send_message_client(e)
    def on_statsyline(self,c,e):
        print('on_statsyline', e)
        self.send_message_client(e)
    def on_endofstats(self,c,e):
        print('on_endofstats', e)
        self.send_message_client(e)
    def on_statslline(self,c,e):
        print('on_statslline', e)
        self.send_message_client(e)
    def on_statsuptime(self,c,e):
        print('on_statsuptime', e)
        self.send_message_client(e)
    def on_statsoline(self,c,e):
        print('on_statsoline', e)
        self.send_message_client(e)
    def on_statshline(self,c,e):
        print('on_statshline', e)
        self.send_message_client(e)     

    def on_time(self,c,e):
        print('on_time', e)
        self.send_message_client(e)

    def on_whoisuser(self,c,e):
        print('on_whoisuser', e)
        self.send_message_client_2(e)
    def on_whoisserver(self,c,e):
        print('on_whoisserver', e)
        self.send_message_client_2(e)
    def on_whoisoperator(self,c,e):
        print('on_whoisoperator', e)
        self.send_message_client_2(e)
    def on_whowasuser(self,c,e):
        print('on_whowasuser', e)
        self.send_message_client_2(e)
    def on_endofwho(self,c,e):
        print('on_endofwho', e)
        self.send_message_client_2(e)
    def on_whoischanop(self,c,e):
        print('on_whoischanop', e)
        self.send_message_client_2(e)
    def on_whoisidle(self,c,e):
        print('on_whoisidle', e)
        self.send_message_client_2(e)
    def on_endofwhois(self,c,e):
        print('on_endofwhois', e)
        self.send_message_client_2(e)
    def on_whoischannels(self,c,e):
        print('on_whoischannels', e)
        self.send_message_client_2(e)
    def on_whoisaccount(self,c,e):
        print('on_whoisaccount', e)
        self.send_message_client_2(e)

    def run(self):
        t = threading.Thread(target=self.start)
        t.daemon = True
        t.start()

    def send_message_client(self, e):
        msg = {}
        msg["nick"] = e.source
        msg["target"] = e.target
        msg["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
        msg["message"] = " ".join(e.arguments)
        self.callback(json.loads(json.dumps(msg)), 'recived_message', self.sid)

    def send_message_client_2(self, e):
        msg = {}
        msg["nick"] = e.source
        msg["target"] = e.target
        msg["time"] = strftime("%d-%m-%Y %H:%M:%S", gmtime())
        msg["message"] = " - ".join(e.arguments)
        self.callback(json.loads(json.dumps(msg)), 'recived_message', self.sid)

    def send_server_message_client(self, msg):
        self.callback(msg, 'server_message', self.sid)
    
    

if __name__ == "__main__":
    reactor = IRCClient(target='#ufg', callback='x')
    reactor.run()
    while True:
        reactor.connection.privmsg(reactor.target, 'olá')
        time.sleep(0.5)
