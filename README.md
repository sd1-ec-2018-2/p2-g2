# Projeto 2

## Membros

* Lucas Simões Passos - lucassimoe@gmail.com - Líder
* Bruna Larissa Camara Queiroz - bruna@tudofacil.net - desenvolvedor
* Antonio Igor Rodrigues Dourado - antonio_igorr@hotmail.com - desenvolvedor
* Werikcyano Lima Guimaraes - werikguimaraes2014@hotmail.com - desenvolvedor
* Andre Luiz Cardoso da Costa - mr.luizandre@gmail.com - desenvolvedor
* Eduardo Garcia - edusantosgarcia@gmail.com - desenvolvedor


## Instalação

Requerimentos

    * Python 3

Depedencias

```bash
pip3 install -r requirements.txt
```

Iniciar servidor

```bash
python3 server.py
```

Servidor vai iniciar em:

```
http://localhost:5000
```